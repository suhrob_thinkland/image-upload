from rest_framework import viewsets, pagination

# Create your views here.
from main.models import Image
from main.serializers import ImageSerializer


class ImageView(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    pagination_class = pagination.LimitOffsetPagination
